const _ = require('lodash')
const bodyParser = require('body-parser')
const express = require('express')
const {listVehicles, vehicleInfo, setHVAC} = require('./tesla')
const minimist = require('minimist')

const app = express()
app.use(bodyParser.urlencoded({extended: false}))

app.post('/slack', async (req, res) => {
  if (req.body.token != process.env.SLACK_RECEIVE_TOKEN) {
    res.sendStatus(403)
    return
  }

  const cmdline = minimist(req.body.text.trim().split(' '))

  // --hvac on|off
  const cmdHVAC = cmdline.hvac
  const cmdHelp = cmdline.help

  if (cmdHelp != null) {
      res.json(toSlackMessage(`usage: !tesla [--hvac on|off]`))
  } else {
    try {
      const vehicles = await listVehicles(process.env.TESLA_USERNAME, process.env.TESLA_PASSWORD)
      if (vehicles.length == 0) {
	res.json(toSlackMessage(`Sorry, no vehicles found`))
      } else {
	const vehicle = vehicles[0];
	if (cmdHVAC != null) {
	  var wantState;
	  if (cmdHVAC == "on") {
	    wantState = true;
	  } else if (cmdHVAC == "off") {
	    wantState = false;
	  }
	  if (wantState != null) {
	    const success = await setHVAC(vehicle, wantState);
	    res.json(toSlackMessage((success ? "HVAC set" : "Failed to set HVAC") + " " + (wantState ? "on" : "off")))
	  } else {
	    res.json(toSlackMessage("usage: --hvac on|off"))
	  }
	} else {
	  const infoLines = await vehicleInfo(vehicle)
	  const response = [`Vehicle: ${vehicle.displayName}`].concat(infoLines).join('\n')
	  res.json(toSlackMessage(response))
	}
      }
    } catch (err) {
      console.error(err)
      res.json(toSlackMessage(`Tesla API call failed: ${err.body.error}`))
    }
  }
})

// used for newrelic monitoring at Heroku
app.get('/', (req, res) => {
  res.send('ok')
})

const port = process.env.PORT || 5000
app.listen(port, () => console.log(`Listening on *:${port}`))

function toSlackMessage(text) {
  return {text, username: process.env.SLACK_BOT_NAME || 'Tesla Bot'}
}
